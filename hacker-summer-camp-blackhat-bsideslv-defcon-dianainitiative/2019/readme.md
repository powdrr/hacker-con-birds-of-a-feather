## Resources for 2019 Hacker Summer Camp

### What is Hacker Summer Camp?
It's the time period when many Information Security / Cyber Security / Hacker Conferences occur in Las Vegas, Nevada, USA

https://www.blackhat.com/ 
https://bsideslv.org
https://defcon.org/
https://www.queercon.org/ 
https://www.dianainitiative.org/ 

### What is Birds of a Feather

We are trying to gather a list of as many resources, events and programs as possible occuring at summer camp and provide links to them to help you create the best time for you. We will try and tag the events with who the event or resource is aimed at, who is welcome, who is discouraged, so you can flock together with other birds like you.

### Chats / Discussion areas

- https://discord.gg/PUGjXBp - DEFCON27-squad - Women focused, non-binary welcome Discord

### Social Media

- https://twitter.com/womenofdefcon - Women focused, non-binary welcome
- https://twitter.com/queercon - they will be having non-binary focused events with their partner
- https://twitter.com/defconparties - http://defconparties.com/ 

### Individual Events / Programs

- https://www.queercon.org/ - open to all, running special non-binary events
- https://www.dianainitiative.org/ - open to all, women focused

#### *Buddy Program*
For: Newbies

Link: [Buddy Program Read Me](https://gitlab.com/CircuitSwan/hacker-con-birds-of-a-feather/blob/master/hacker-summer-camp-blackhat-bsideslv-defcon-dianainitiative/2019/buddy-system.md)

#### *Brunch*

- Details: https://docs.google.com/document/d/1TKa-gxqOVuvYRYv2GclPFT0wYJeNYHEt7xoZ4F3d7YI/edit
- For: For the women and non-binary folkx

#### *Spa Day*

Women focused, non-binary welcome, details in TDI Slack

#### *Photoshoot*

Women focused, non-binary welcome

TBD

### Swag Planning

- (Stickers)[https://docs.google.com/document/d/1fXjhw6pgzoev1rSDdWWXg4lwbazvD2guAIyTSLkaMEM/edit#]
- (Shirts)[https://docs.google.com/document/d/1DxrQwqwr4bphKu-wBOXv_wMi1xHHf5yDQJspuOvWJRM/edit#heading=h.kg7t1yy9xjfd]

### Useful Applications

- https://hackertracker.app/ 

### Conference guides / tips-and-tricks

- https://sites.google.com/site/amazonv/first-conference?authuser=0

### Other Resource compliations (like this)