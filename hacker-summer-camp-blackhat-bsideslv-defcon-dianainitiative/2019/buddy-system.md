# Buddy system

Sometimes it is great to explore a con with a group of people who will watch one anothers backs, or just are interested in new things.

To work best it might be good to talk before the event.

What data you enter into the form should be public, keep OPSEC in mind

Feel free to buddy with other newbies!

Once you find a buddy (1 or more) reach out before the event in the correct medium (twitter, discord) to connect.

Remember to be cautious and meet in a public place, there is no vetting in place. If you can please try to do your own vetting.

You’ll probably want to try and find people with the same interests so you can go to the same talks and villages and events.

Veterans are encouraged to find a buddy and help shown them around and introduce them around.

First timer tips: https://sites.google.com/site/amazonv/first-conference 

Join the Women and Non-Binary squad discord to get access to the buddy sign up - https://discord.gg/PUGjXBp 
